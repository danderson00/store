var memory = require('./memory')

module.exports = function () {
  // try {
  //   var moduleName = '@x/store.sqlite.react-native'
  //   require.resolve('react-native')
  //   return require(moduleName)
  // } catch(error) {
    if((typeof window !== 'undefined' && window.indexedDB !== undefined) || (typeof self !== 'undefined' && self.indexedDB !== undefined))
      // if we are in a browser environment, use indexeddb
      return require('@x/store.indexeddb')
    else if (typeof process !== 'undefined' && process.versions && process.versions.node)
      // if we are in a node environment, we can just try to require up the default provider
      try {
        var moduleName = '@x/store.sql'
        return require(moduleName)
      } catch(error) {
        console.warn('Unable to load default provider for node (@x/store.sql)')
        return memory
      }
    else {
      return memory
    }
  // }
}
