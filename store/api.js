module.exports = function (defaultProvider) {
    return function (options) {
        options = options || {}
        return {
            open: function (entities) {
                return open(options.provider || defaultProvider, entities, options)
            }
        }
    }
}

function open(provider, initialEntities, options) {
    var store = provider(options)

    return internalOpen(initialEntities)

    function internalOpen(entities) {
        return store.open(entities).then(function () {
            var hash = {}
            for (var i = 0, l = entities.length; i < l; i++)
                hash[entities[i].name] = entities[i]

            return {
                entity: function (name) {
                    return store.entityContainer(hash[name])
                },
                close: function () {
                    if (store.close) return store.close()
                },
                reopen: function (newEntities) {
                    return internalOpen(newEntities)
                }
            }
        })
    }
}
