﻿var environment = require('./environment')
var api = require('./api')

module.exports = api(environment())
