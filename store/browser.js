var provider = require('@x/store.indexeddb')
var api = require('./api')

module.exports = api(provider)
