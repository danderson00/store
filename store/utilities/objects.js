module.exports = {
    indexOnProperty: function (property, source) {
        // really should use .reduce and polyfill if needed
        var result = {};
        for(var i = 0, l = source.length; i < l; i++)
            result[source[i][property]] = source[i];
        return result;
    },
    pluck: function (array, property) {
        return array.map(function (item) {
            return item[property];
        });
    },
    flatten: function flatten() {
        var flat = [];
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] instanceof Array) {
                flat.push.apply(flat, flatten.apply(this, arguments[i]));
            } else {
                flat.push(arguments[i]);
            }
        }
        return flat;
    },
    unique: function(a) {
        return a.reduce(function(p, c) {
            if (p.indexOf(c) < 0) p.push(c);
            return p;
        }, []);
    }
}
