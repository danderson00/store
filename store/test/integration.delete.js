var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.delete', function () {
        var storage = require('..')(options),
            db, entity

        it("deletes entities matching single index", function () {
            return setup()
                .then(function () {
                    return entity.delete({ p: 'p2', v: 'test' })
                })
                .then(function () {
                    return entity.retrieve()
                })
                .then(function (results) {
                    expect(results.length).to.equal(1)
                })
        })

        it("deletes entities matching multiple indexes", function () {
            return setup()
                .then(function () {
                    return entity.delete([{ p: 'p1', v: 2 }, { p: 'p2', v: 'test' }])
                })
                .then(function () {
                    return entity.retrieve()
                })
                .then(function (results) {
                    expect(results.length).to.equal(3)
                })
        })

        afterEach(function () {
            db.close()
        })

        function setup() {
            var tableName = id()
            return storage.open([{ name: tableName, indexes: [['p1', 'p2'], 'p2'] }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store([
                        { p1: 1, p2: 'test' },
                        { p1: 2, p2: 'test' },
                        { p1: 3, p2: 'test2' },
                        { p1: 4, p2: 'test' },
                    ])
                })
        }
    })
  }