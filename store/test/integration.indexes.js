var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.indexes', function () {
        var storage = require('..')(options),
            db

        it("single property indexes can be specified and requested as arrays or individually", function () {
            var container

            return open(['p1', ['p2']], [
                    { p1: 1, p2: 1 },
                    { p1: 2, p2: 2 }
                ])
                .then(function (result) {
                    container = result
                    return container.retrieve({ p: 'p1', v: 2 })
                })
                .then(function (results) {
                    expect(results.length).to.equal(1)
                })
                .then(function (result) {
                    return container.retrieve([{ p: 'p1', v: 2 }])
                })
                .then(function (results) {
                    expect(results.length).to.equal(1)
                })
                .then(function (result) {
                    return container.retrieve({ p: 'p2', v: 2 })
                })
                .then(function (results) {
                    expect(results.length).to.equal(1)
                })
                .then(function (result) {
                    return container.retrieve([{ p: 'p2', v: 2 }])
                })
                .then(function (results) {
                    expect(results.length).to.equal(1)
                })
        })

        it("retrieve sorts by index properties", function () {
            var container

            return open([['p1', 'p2.value'], ['p2.value', 'p1']], [
                    { p1: 4, p2: { value: 1 } },
                    { p1: 3, p2: { value: 2 } },
                    { p1: 1, p2: { value: 1 } },
                    { p1: 2, p2: { value: 2 } }
                ])
                .then(function (result) {
                    container = result
                    return container.retrieve([{ p: 'p1', o: '>', v: 0 }, { p: 'p2.value', o: '>', v: 0 }])
                })
                .then(function (results) {
                    expect(results).to.deep.equal([
                        { p1: 1, p2: { value: 1 } },
                        { p1: 2, p2: { value: 2 } },
                        { p1: 3, p2: { value: 2 } },
                        { p1: 4, p2: { value: 1 } }
                    ])
                    return container.retrieve([{ p: 'p2.value', o: '>', v: 0 }, { p: 'p1', o: '>', v: 0 }])
                })
                .then(function (results) {
                    expect(results).to.deep.equal([
                        { p1: 1, p2: { value: 1 } },
                        { p1: 4, p2: { value: 1 } },
                        { p1: 2, p2: { value: 2 } },
                        { p1: 3, p2: { value: 2 } }
                    ])
                })
        })

        it("indexes with array values can be queried by individual value", function () {
            return open(['p1'],
                [
                    { p1: [1, 2], p2: 'test' },
                    { p1: [2, 3], p2: 'test2' }
                ])
                .then(function (container) {
                    return container.retrieve({ p: 'p1', v: 1 }).then(function (rows) {
                        expect(rows.length).to.equal(1)
                        expect(rows[0]).to.deep.equal({ p1: [1, 2], p2: 'test' })
                        return container.retrieve({ p: 'p1', v: 2 })
                    })
                    .then(function (rows) {
                        expect(rows.length).to.equal(2)
                    })
                })
        })

        it("indexes with multiple array values can be queried by individual value", function () {
            return open([['p1', 'p2']],
                [
                    { p1: [1, 2], p2: [1, 3] },
                    { p1: [2, 3], p2: [2, 1] },
                    { p1: [3, 1], p2: [3, 2] }
                ])
                .then(function (container) {
                    return container.retrieve([{ p: 'p1', v: 2 }, { p: 'p2', v: 1 }])
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(2)
                })
        })

        // support for undefined can be implemented in sql using a special query that detects if the index value is not present
        // it("indexes can handle undefined values", function () {
        //     return open(['p1'],
        //       [
        //           { p1: 1, p2: 'test' },
        //           { p2: 'test2' }
        //       ])
        //       .then(function (container) {
        //           return container.retrieve({ p: 'p1', v: undefined })
        //       })
        //       .then(function (rows) {
        //           expect(rows.length).to.equal(1)
        //           expect(rows[0]).to.deep.equal({ p2: 'test2' })
        //       })
        // })

        it("indexes can handle null values", function () {
            return open(['p1'],
              [
                  { p1: 1, p2: 'test' },
                  { p1: null, p2: 'test2' }
              ])
              .then(function (container) {
                  return container.retrieve({ p: 'p1', v: null })
              })
              .then(function (rows) {
                  expect(rows.length).to.equal(1)
                  expect(rows[0]).to.deep.equal({ p1: null, p2: 'test2' })
              })
        })

        it("indexes are updated correctly", function () {
            return open(['p1'],
                [
                    { p1: 1 },
                    { p1: 2 }
                ], 'p1')
                .then(function (container) {
                    return container.store({ p1: 1 })
                        .then(function () {
                            return container.retrieve({ p: 'p1', v: 1 })
                        })
                        .then(function (rows) {
                            expect(rows.length).to.equal(1)
                        })
                })
        })

        // these pass on sql but fail on indexeddb due to reopen not working, and cause problems with subsequent tests
        it("indexes are updated on creation", function () {
            var tableName = id()
            var rows = Array(50).fill(null).map(function (entry, index) {
                return {
                    p1: index % 10
                }
            })

            return open([], rows, undefined, tableName)
                .then(function (entity) {
                    return db.reopen([{ name: tableName, indexes: ['p1'] }])
                        .then(function () {
                            return entity.retrieve({ p: 'p1', v: 2 })
                        })
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(5)
                })
        })

        it("newly created indexes have undefined values set to null on creation", function () {
            var tableName = id()
            var rows = [{
                id: '1'
            }]

            return open([], rows, 'id', tableName)
              .then(function (entity) {
                  return db.reopen([{ name: tableName, indexes: ['p1'], keyPath: 'id' }])
                    .then(function () {
                        return entity.retrieve({ p: 'p1', v: null })
                    })
              })
              .then(function (rows) {
                  expect(rows.length).to.equal(1)
              })
        })

        function open(indexes, entities, keyPath, tableName) {
            tableName = tableName || id()

            var entity

            return storage.open([{ name: tableName, indexes: indexes, keyPath: keyPath }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store(entities)
                })
                .then(function () {
                    return entity
                })
        }

        afterEach(function () {
            db.close()
        })
    })
}
