var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.autoIncrement', function () {
        var storage = require('..')(options),
            db

        it("store operation returns entity with autoIncrement keyPath property set", function () {
            return open([], [], 'id')
                .then(function (container) {
                    return container.store({})
                })
                .then(function (updatedEntity) {
                    expect(updatedEntity).to.deep.equal({ id: 1 })
                })
        })

        it("multiple store operation returns entities with autoIncrement keyPath property set", function () {
            return open([], [], 'id')
                .then(function (container) {
                    return container.store([{}, {}])
                })
                .then(function (updatedEntity) {
                    expect(updatedEntity).to.deep.equal([{ id: 1 }, { id: 2 }])
                })
        })

        it("stored entity has autoIncrement keyPath property set", function () {
            var container
            return open([], [], 'id')
                .then(function (db) {
                    container = db
                    return container.store({})
                })
                .then(function () {
                    return container.retrieve({ p: 'id', v: 1 })
                })
                .then(function (entities) {
                    expect(entities.length).to.equal(1)
                    expect(entities[0]).to.deep.equal({ id: 1 })
                })
        })

        function open(indexes, entities, keyPath) {
            var entity
            var tableName = id()

            // simplified behavior is to always autoIncrement
            return storage.open([{ name: tableName, indexes: indexes, keyPath: keyPath, autoIncrement: true }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store(entities)
                })
                .then(function () {
                    return entity
                })
        }

        afterEach(function () {
            db.close()
        })
    })
}
