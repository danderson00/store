var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.expressions', function () {
        var storage = require('..')(options),
            db

        it("equality 1", function () {
            return retrieve({ p: 'p1', o: '=', v: 1 }, [1])
        })
        it("equality 2", function () {        
            return retrieve([{ p: 'p1', o: '=', v: 1 }, { p: 'p2', o: '=', v: 1 }, { p: 'p3', o: '=', v: 'test' }], [1])
        })
        it("equality 3", function () {        
            return retrieve([{ p: 'p1', o: '=', v: 1 }, { p: 'p2', o: '=', v: 2 }, { p: 'p3', o: '=', v: 'test' }], [])
        })

        it("greaterThan 1", function () {
            return retrieve({ p: 'p1', o: '>', v: 2 }, [3, 4])
        })
        it("greaterThan 2", function () {
            return retrieve([{ p: 'p1', o: '>', v: 1 }, { p: 'p2', o: '>', v: 2 }, { p: 'p3', o: '=', v: 'test' }], [4])
        })
        it("greaterThan 3", function () {
            return retrieve([{ p: 'p1', o: '>', v: 1 }, { p: 'p2', o: '>', v: 4 }, { p: 'p3', o: '=', v: 'test' }], [])
        })

        it("greaterThanEqualTo 1", function () {
            return retrieve({ p: 'p1', o: '>=', v: 2 }, [2, 3, 4])
        })
        it("greaterThanEqualTo 2", function () {
            return retrieve([{ p: 'p1', o: '>=', v: 2 }, { p: 'p2', o: '>=', v: 1 }, { p: 'p3', o: '=', v: 'test' }], [2, 4])
        })
        it("greaterThanEqualTo 3", function () {
            return retrieve([{ p: 'p1', o: '>=', v: 1 }, { p: 'p2', o: '>=', v: 4 }, { p: 'p3', o: '=', v: 'test' }], [4])
        })

        it("lessThan 1", function () {
            return retrieve({ p: 'p1', o: '<', v: 3 }, [1, 2])
        })
        it("lessThan 2", function () {
            return retrieve([{ p: 'p1', o: '<', v: 4 }, { p: 'p2', o: '<', v: 3 }, { p: 'p3', o: '=', v: 'test' }], [1, 2])
        })
        it("lessThan 3", function () {
            return retrieve([{ p: 'p1', o: '<', v: 4 }, { p: 'p2', o: '<', v: 1 }, { p: 'p3', o: '=', v: 'test' }], [])
        })

        it("lessThanEqualTo 1", function () {
            return retrieve({ p: 'p1', o: '<=', v: 3 }, [1, 2, 3])
        })
        it("lessThanEqualTo 2", function () {
            return retrieve([{ p: 'p1', o: '<=', v: 4 }, { p: 'p2', o: '<=', v: 3 }, { p: 'p3', o: '=', v: 'test' }], [1, 2])
        })
        it("lessThanEqualTo 3", function () {
            return retrieve([{ p: 'p1', o: '<=', v: 4 }, { p: 'p2', o: '<=', v: 1 }, { p: 'p3', o: '=', v: 'test' }], [1])
        })

        it("using api", function () {
            return retrieve(x => x.where('p1', '=', 1).and('p2', '=', 1).and('p3', '=', 'test'), [1])
        })

        function retrieve(expression, resultIds) {
            var entity
            var tableName = id()

            return storage.open([{ name: tableName, indexes: ['p1', ['p1', 'p2', 'p3']] }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store([
                        { p1: 1, p2: 1, p3: 'test' },
                        { p1: 2, p2: 2, p3: 'test' },
                        { p1: 3, p2: 3, p3: 'test2' },
                        { p1: 4, p2: 4, p3: 'test' },
                    ])
                })
                .then(function () {
                    return entity.retrieve(expression)
                })
                .then(function (results) {
                    expect(results.length).to.equal(resultIds.length)
                    for (var i = 0, l = resultIds.length; i < l; i++)
                        expect(results[i].p1).to.equal(resultIds[i])
                })
        }

        afterEach(function () {
            db.close()
        })
    })

}
