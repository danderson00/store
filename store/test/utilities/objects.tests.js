var expect = require('chai').expect;

describe('store.utilities.objects', function () {
    var objects = require('../../utilities/objects');

    it("indexOnProperty", function () {
        expect(objects.indexOnProperty('p1', [
            { p1: 'a', p2: 1 },
            { p1: 'b', p2: 2 }
        ])).to.deep.equal({
            a: { p1: 'a', p2: 1 },
            b: { p1: 'b', p2: 2 }
        });
    });

    it("pluck", function () {
        expect(objects.pluck([
            { p1: 'a', p2: 1 },
            { p1: 'b', p2: 2 }
        ], 'p1')).to.deep.equal(['a', 'b']);
    });

    it("flatten", function () {
        expect(objects.flatten([
                [{a:1}, {a:2}],
                {a:3},
                [[{a:4}, {a:5}], {a:6}]
            ], 
            [{a:7}])
        ).to.deep.equal([{a:1},{a:2},{a:3},{a:4},{a:5},{a:6},{a:7}]);
    });

    it("unique", function () {
        var o = {}
        expect(objects.unique([1, 2, 3, o, 2, 2, o]))
            .to.deep.equal([1, 2, 3, o])
    })
});
