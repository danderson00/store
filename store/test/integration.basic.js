var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.basic', function () {
        var storage = require('..')(options),
            db

        it("basic store and retrieve", function () {
            return open(['p1', 'p2'],
                [
                    { p1: 1, p2: 'test' },
                    { p1: 2, p2: 'test2' }
                ])
                .then(function (container) {
                    return container.retrieve({ p: 'p1', v: 1 })
                        .then(function (rows) {
                            expect(rows.length).to.equal(1)
                            expect(rows[0]).to.deep.equal({ p1: 1, p2: 'test' })
                            return container.retrieve()
                        })
                        .then(function (rows) {
                            expect(rows.length).to.equal(2)
                        })
                })
        })

        it("multiple key index store and retrieve", function () {
            return open([['p1', 'p2']],
                [
                    { p1: 'test', p2: 1 },
                    { p1: 'test', p2: 2 },
                    { p1: 'test', p2: 3 },
                    { p1: 'test2', p2: 2 },
                ])
                .then(function (container) {
                    return container.retrieve([{ p: 'p1', v: 'test' }, { p: 'p2', o: '>=', v: 2 }])
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(2)
                })
        })

        it("complex object store and retrieve", function () {
            return open([['p1.p2', 'p3']],
                [
                    { p1: { p2: 'test' }, p3: 1 },
                    { p1: { p2: 'test' }, p3: 1 },
                    { p1: { p2: 'test2' }, p3: 1 }
                ])
                .then(function (container) {
                    return container.retrieve([{ p: 'p1.p2', v: 'test' }, { p: 'p3', v: 1 }])
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(2)
                })
        })

        it("keyPath can be queried", function () {
            return open([],
                [
                    { p1: 1, p2: 'test1' },
                    { p1: 2, p2: 'test2' },
                    { p1: 3, p2: 'test3' }
                ], 'p1')
                .then(function (container) {
                    return container.retrieve({ p: 'p1', o: '>=', v: 2 })
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(2)
                    expect(rows).to.deep.equal([{ p1: 2, p2: 'test2' }, { p1: 3, p2: 'test3' }])
                })
        })

        it("keyPath can be queried with other predicates", function () {
            return open(['p2'],
                [
                    { p1: 1, p2: 'test1' },
                    { p1: 2, p2: 'test2' },
                    { p1: 3, p2: 'test3' }
                ], 'p1')
                .then(function (container) {
                    return container.retrieve([{ p: 'p1', o: '>=', v: 2 }, { p: 'p2', v: 'test3' }])
                })
                .then(function (rows) {
                    expect(rows.length).to.equal(1)
                    expect(rows).to.deep.equal([{ p1: 3, p2: 'test3' }])
                })
        })

        it("updates existing entities", function () {
            return open(['p2'],
                [
                    { p1: 1, p2: 'test1' },
                    { p1: 2, p2: 'test2' },
                    { p1: 3, p2: 'test3' }
                ], 'p1')
                .then(function (container) {
                    return container.store({ p1: 1, p2: 'test4' })
                        .then(function () {
                            return container.retrieve({ p: 'p1', o: '=', v: 1 })
                        })
                        .then(function (rows) {
                            expect(rows).to.deep.equal([{ p1: 1, p2: 'test4' }])
                            return container.store([
                                { p1: 1, p2: 'new' },
                                { p1: 4, p2: 'new' }
                            ])
                        })
                        .then(function () {
                            return container.retrieve({ p: 'p2', o: '=', v: 'new' })
                        })
                        .then(function (rows) {
                            expect(rows).to.deep.equal([
                                { p1: 1, p2: 'new' },
                                { p1: 4, p2: 'new' }
                            ])
                        })
                })
        })

        function open(indexes, entities, keyPath) {
            var entity
            var tableName = id()

            return storage.open([{ name: tableName, indexes: indexes, keyPath: keyPath }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store(entities)
                })
                .then(function () {
                    return entity
                })
        }

        afterEach(function () {
            db.close()
        })
    })
}
