var expect = require('chai').expect
var id = require('uuid').v4

module.exports = function (options, describe, it, afterEach) {
    describe('store.integration.misc', function () {
        var storage = require('..'),
            db

        it("clear deletes all entities", function () {
            var container

            return open(['p1'], [{ p1: 1, p2: 1 }])
                .then(function (result) {
                    container = result
                    return container.clear()
                })
                .then(function () {
                    return container.retrieve({ p: 'p1', v: 1 })
                })
                .then(function (messages) {
                    expect(messages.length).to.equal(0)
                })
        })

        // this is not a good test and fails on MySQL
        // it("writes are atomic", function () {
        //     return open(['p1'], [{ p1: 1, p2: 1 }])
        //         .then(function (entity) {
        //             entity.store({ p1: 1, p2: 'test2' })
        //             return entity.retrieve({ p: 'p1', v: 1 })
        //         })
        //         .then(function (results) {
        //             expect(results.length).to.equal(2)
        //         })
        // })

        it("databases are isolated by provided name", function () {
            return open(['p1'], [{ id: 1, p1: 1 }], 'db1')
                .then(function (entity1) {
                    return open(['p1'], [{ id: 2, p1: 1 }], 'db1')
                        .then(function (entity2) {
                            return entity1.store({ id: 3, p1: 1 })
                                .then(function () {
                                    return entity1.retrieve({ p: 'p1', v: 1 })
                                })
                                .then(function (results) {
                                    expect(results.length).to.equal(2)
                                    return entity2.retrieve({ p: 'p1', v: 1 })
                                })
                                .then(function (results) {
                                    expect(results.length).to.equal(1)
                                })
                        })
                })
        })

        function open(indexes, entities, name) {
            var entity
            var allOptions = Object.assign({ name: name }, options)
            var tableName = id()

            return storage(allOptions).open([{ name: tableName, indexes: indexes }])
                .then(function (provider) {
                    db = provider
                    entity = provider.entity(tableName)
                    return entity.store(entities)
                })
                .then(function () {
                    return entity
                })
        }

        afterEach(function () {
            db.close()
        })
    })
}
