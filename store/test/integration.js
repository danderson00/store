module.exports = function (options, describe, it, afterEach) {
  require('./integration.basic')(options, describe, it, afterEach)
  require('./integration.expressions')(options, describe, it, afterEach)
  require('./integration.indexes')(options, describe, it, afterEach)
  require('./integration.autoIncrement')(options, describe, it, afterEach)
  require('./integration.delete')(options, describe, it, afterEach)
  require('./integration.misc')(options, describe, it, afterEach)
}
