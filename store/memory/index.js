var entityContainer = require('./entityContainer');

module.exports = function (entities, options) {
    return {
        name: '@x/store.memory',
        open: function () {
            // no initialisation required, just return a resolved promise
            return Promise.resolve();
        },
        entityContainer: function (entity) {
            return entityContainer(entity);
        }
    };
};
