var deleteDb = require('./db/delete');

module.exports = function (options) {
    return deleteDb('__metadata').then(function () {
        return deleteDb(options.name || '__entities');
    });
};
