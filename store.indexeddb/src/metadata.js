// we need to store metadata about entities so we know when to upgrade the store
var open = require('./db/open'),
    store, db;

module.exports = function (options, version, entities) {
    var retrieve = arguments.length !== 3;
    return Promise.resolve(initialise()).then(function () {
        if (retrieve) {
            return store.single(options.name || '__entities');
        } else {
            var data = { name: options.name || '__entities', version: version, entities: entities };
            return store.put(data).then(function () {
                return data;
            });
        }
    });
};

module.exports.close = function () {
    db && db.close();
    store = undefined;
    db = undefined;
};

function initialise() {
    if (!store)
        return open('__metadata', 1, function (database) {
            database.createObjectStore('stores', { keyPath: 'name' });
        }).then(function (database) {
            db = database;
            store = db.store({ name: 'stores' });
        });
}
