module.exports = function (db, name, args) {
    return evaluateCursor(db.transaction([name]).objectStore(name).openCursor(args));
};

module.exports.index = function (db, store, index, keyRange) {
    return evaluateCursor(db.transaction([store]).objectStore(store).index(index).openCursor(keyRange));
};

module.exports.index.delete = function (db, store, index, keyRange) {
    return deleteCursor(db.transaction([store], 'readwrite').objectStore(store).index(index).openCursor(keyRange));    
}

function evaluateCursor(cursor) {
    return new Promise(function (resolve, reject) {
        var results = [];

        cursor.onsuccess = function (event) {
            var result = event.target.result;
            if (result) {
                results.push(result.value);
                result.continue();
            }
            else
                resolve(results);
        };

        cursor.onerror = function (event) {
            reject(this.error);
        };
    });
};

function deleteCursor(cursor) {
    return new Promise(function (resolve, reject) {
        var results = [];

        cursor.onsuccess = function (event) {
            var result = event.target.result;
            if (result) {
                result.delete();
                result.continue();
            }
            else
                resolve(results);
        };

        cursor.onerror = function (event) {
            reject(this.error);
        };
    });
}
