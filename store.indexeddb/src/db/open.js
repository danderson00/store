var store = require('./store');

module.exports = function (name, version, upgrade) {
    return module.exports.db(name, version, upgrade).then(function (database) {
        return {
            store: function (entity) {
                return store(database, entity);
            },
            close: function () {
                database.close();
            }
        };
    });
};

module.exports.db = function (name, version, upgrade) {
    return new Promise(function (resolve, reject) {
        var global = typeof window === 'undefined' ? self : window,
            req = global.indexedDB.open(name, version);

        req.onupgradeneeded = function (event) {
            upgrade(event.target.result, event.currentTarget.transaction);
        };

        req.onsuccess = function (event) {
            resolve(event.target.result);
        };

        req.onerror = function (event) {
            reject(this.error);
        };
    })
}