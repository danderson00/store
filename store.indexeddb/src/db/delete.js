module.exports = function (name) {
    return new Promise(function (resolve, reject) {
        var global = typeof window === 'undefined' ? self : window,
            request = global.indexedDB.deleteDatabase(name);

        //request.oncomplete = resolve;
        request.onsuccess = function () { resolve(); };
        request.onerror = function () { resolve(); };
        request.onblocked = function () { resolve(); };
    });
};
