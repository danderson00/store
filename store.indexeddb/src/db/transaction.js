module.exports = function (transaction) {
    return new Promise(function (resolve, reject) {
        transaction.oncomplete = function (event) {
            resolve(event.target.result);
        };

        transaction.onerror = function (event) {
            var error = this.error || new Error(event.target.error.message);
            reject(error);
        };
    });
};
