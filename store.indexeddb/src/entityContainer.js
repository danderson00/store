var indexes = require('./indexes'),
    keyPath = require('@x/store.expressions/src/keyPath'),
    api = require('@x/store.expressions/src/api');

module.exports = function (store, definition) {
    return {
        store: function (entity) {
            return store.put(entity).then(function (result) {
                if (definition.keyPath && definition.autoIncrement) {
                    if (result.constructor === Array)
                        for (var i = 0, l = result.length; i < l; i++)
                            keyPath.set(definition.keyPath, entity[i], result[i]);
                    else
                        keyPath.set(definition.keyPath, entity, result);
                }
                return entity;
            });
        },
        retrieve: function (expression) {
            if(!expression)
                return store.all();

            if(expression.constructor === Function)
                expression = api(expression)
            
            if (expression.constructor === Array && expression.length === 1)
                expression = expression[0];

            return store.index(indexes.indexName(expression), indexes.convertExpression(expression));
        },
        delete: function (expression) {
            if(!expression)
                return store.all();

            if(expression.constructor === Function)
                expression = api(expression)
            
            if (expression.constructor === Array && expression.length === 1)
                expression = expression[0];

            return store.delete(indexes.indexName(expression), indexes.convertExpression(expression));
        },
        clear: function () {
            return store.clear();
        }
    };
};
