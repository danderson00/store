var open = require('./db/open'),
    reset = require('./reset'),
    metadata = require('./metadata'),
    upgrade = require('./upgrade'),
    entityContainer = require('./entityContainer'),
    entitiesHaveChanged = require('@x/store/utilities/metadata').entitiesHaveChanged;

module.exports = function (options) {
    options = options || {};

    var db;

    return {
        name: '@x/store.indexeddb',
        open: function (entities) {
            options = options || {};

            return Promise.resolve(resetIfRequired())
                .then(function () {
                    return metadata(options);
                })
                .then(function (oldData) {
                    if(!oldData || entitiesHaveChanged(oldData.entities, entities)) {
                        var version = (oldData ? oldData.version : 0) + 1;

                        return open(options.name || '__entities', version, upgrade(entities, oldData && oldData.entities))
                            .then(function (provider) {
                                return metadata(options, version, entities).then(function () {
                                    return provider;
                                });
                            });
                    } else {
                        return open(oldData.name, oldData.version);
                    }
                })
                .then(function (database) {
                    db = database;
                });

            function resetIfRequired() {
                if (options.reset)
                    return reset(options);
            }
        },
        entityContainer: function (entity) {
            return entityContainer(db.store(entity), entity);
        },
        close: function () {
            db && db.close();
            metadata.close();
        }
    };
}