var expect = require('chai').expect;

describe('store.indexeddb.metadata', function () {
    var metadata = require('../src/metadata');

    it("metadata can be retrieved once set", function () {
        return metadata({ name: 'test' }, 1, [{ name: 'entity' }])
            .then(function (data) {
                expect(data.name).to.equal('test');
                expect(data.version).to.equal(1);
                expect(data.entities[0].name).to.equal('entity');
                return metadata({ name: 'test' })
            })
            .then(function (data) {
                expect(data.name).to.equal('test');
                expect(data.version).to.equal(1);
                expect(data.entities[0].name).to.equal('entity');
                metadata.close();
            });
    });
});
