module.exports = function(config) {
  config.set({
    browsers: ['Chrome'],
    frameworks: ['mocha'],

    files: [
      'test/**/*.tests.js'
    ],

    preprocessors: {
      'test/**/*.tests.js': ['webpack']
    },
    
    webpack: {
      devtool: 'inline-source-map'
    }
  });
};