var expect = require('chai').expect;

suite('store.expressions.apply', function () {
  var api = require('../src/api')

  test("single clause", function () {
    expect(
      api(function (x) { return x.where('p1', '=', '1') })
    ).to.deep.equal(
      [{ p: 'p1', o: '=', v: '1' }]
    )
  })

  test("multiple clauses", function () {
    expect(
      api(function (x) { 
        return x.where('p1', '=', '1').and('p2', '>=', 2)
      })
    ).to.deep.equal(
      [
        { p: 'p1', o: '=', v: '1' },
        { p: 'p2', o: '>=', v: 2 }
      ]
    )
  })
})