var expect = require('chai').expect;

suite('store.expressions.flatten', function () {
    var flatten = require('../src/flatten');

    test("flatten", function () {
        expect(flatten([1, [2, [3, 4], [5]], 6])).to.deep.equal([1, 2, 3, 4, 5, 6])
        expect(flatten([0, 1, [2, [undefined, 4], [null]], 6])).to.deep.equal([0, 1, 2, 4, 6])
    })
})
