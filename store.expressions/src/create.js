module.exports = function (source, prepend) {
    return Object.keys(source).map(function (path) {
        return { p: (prepend ? prepend + '.' : '') + path, v: source[path] };
    });
}

module.exports.fromScope = function (source) {
    if(!source) return;

    return Object.keys(source).map(function (path) {
        // prepend data. unless the path is topic
        return { p: (path === 'topic' ? '' : 'data.') + path, v: source[path] };
    });
}
