var keyPath = require('./keyPath');

module.exports = {
    evaluateKeyPath: keyPath,
    setKeyPath: keyPath.set,
    api: require('./api'),
    evaluate: require('./evaluate'),
    apply: require('./apply'),
    create: require('./create'),
    combine: require('./combine'),
    flatten: require('./flatten')
};
