module.exports = function (definition) {
  var expression = []

  var api = {
    where: function (path, operator, value) {
      expression.push({ p: path, o: operator, v: value })
      return {
        and: api.where
      }
    }
  }

  definition(api)
  return expression
}