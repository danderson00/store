module.exports = function (target) {
    return target.reduce(function (result, item) {
        if(item === undefined || item === null) return result;
        return result.concat(item.constructor === Array ? module.exports(item) : item);
    }, []);
}
