const provider = require('../src')
const expect = require('chai').expect

describe('store.initialisationSql', () => {
  const options = {
    client: 'sqlite',
    connection: { filename: ':memory:' }
  }

  it("generates basic SQL", () => {
    const sql = provider(options).initialisationSql([
      { name: 'withKey', keyPath: 'key' },
      { name: 'withoutKey' },
      { name: 'indexed', indexes: ['p1'] }
    ])
    expect(sql).to.equal('create table `withKey` (`id` varchar(255), `content` text, primary key (`id`))\n' +
      'create table `withoutKey` (`id` integer not null primary key autoincrement, `content` text)\n' +
      'create table `indexed` (`id` integer not null primary key autoincrement, `content` text)\n' +
      'create table `indexed_p1` (`id` integer, `index_id` integer not null primary key autoincrement, `value` varchar(255) null, foreign key(`id`) references `indexed`(`id`) on delete CASCADE)\n' +
      'create index `indexed_p1_id_value_index` on `indexed_p1` (`id`, `value`)')
  })
})