var provider = require('../src')

require('@x/store/test/integration')({
  provider: provider,

  client: 'sqlite',
  connection: {
    filename: ':memory:'
  },

  // client: 'mssql',
  // connection: {
  //   user: "",
  //   password: "",
  //   server: ".database.windows.net",
  //   database: "store",
  //   options: { encrypt: true }
  // }

  // client: 'mysql',
  // connection: {
  //   host : '127.0.0.1',
  //   user : 'root',
  //   password : 'abc123',
  //   database : 'store'
  // }
}, describe, it, afterEach)
