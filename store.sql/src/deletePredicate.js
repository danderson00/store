var safeName = require('./utilities').safeName

module.exports = function (options) {
  return function (table, entityData, predicates) {
    if(predicates) {
      var tableName = safeName(entityData.name, options.name)

      if (predicates.constructor === Array) {
        predicates.forEach(applyPredicate)
      } else {
        applyPredicate(predicates)
      }

      function applyPredicate(predicate) {
        return table.whereIn('id', function () {
          return this
            .from(tableName + '_' + predicate.p)
            .where('value', sqlOperator(predicate), predicate.v)
            .select('id')
        })
      }
    }
    return table
  }
}

// duplicated in applyPredicate
function sqlOperator(predicate) {
  var operator = predicate.o || '='
  if (operator === '=' && (predicate.v === undefined || predicate.v === null))
    return 'IS'
  if (operator === '=' && predicate.v && predicate.v.constructor === Array)
    return 'IN'
  return operator.toUpperCase()
}
