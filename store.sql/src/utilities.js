module.exports = {
  safeName: function (name, discriminator) {
    return (discriminator ? (discriminator + '_') : '') + name.replace(/\./g, '_')
  }
}