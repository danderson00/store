var expressions = require('@x/store.expressions')
var objects = require('@x/store/utilities/objects')
var safeName = require('./utilities').safeName
var selectPredicateModule = require('./selectPredicate')
var deletePredicateModule = require('./deletePredicate')

module.exports = function (options) {
  var selectPredicate = selectPredicateModule(options)
  var deletePredicate = deletePredicateModule(options)

  return {
    store: function (database, entityData, entity) {
      var id = entityData.keyPath && expressions.evaluateKeyPath(entityData.keyPath, entity)
      var table = database(safeName(entityData.name, options.name))
      var deletePromise = Promise.resolve()
      var container = { content: JSON.stringify(entity) }

      if (id) {
        deletePromise = table.where({ id: id }).del()
        container.id = id
      }

      return deletePromise.then(function () {
        return table.returning('id').insert(container).then(function (result) {
          return id || result[0]
        })
      })
    },
    insertIndexes: function (database, entityData, entity, rowid) {
      var columns = objects.unique(objects.flatten(entityData.indexes || []))
      // this "series" pattern is used in a bunch of places, should refactor
      return columns.reduce(
        function (promise, column) {
          return promise.then(function () {
            return indexInsert(column)
          })
        },
        Promise.resolve()
      )

      function indexInsert(index) {
        var value = expressions.evaluateKeyPath(index, entity)
        var values = (value && value.constructor === Array) ? value : [value]
        var indexTableName = safeName(entityData.name + '_' + index, options.name) 
        var table = database(indexTableName)

        return table.where({ id: rowid }).del()
          .then(function () {
            return values.reduce(
              function (promise, value) {
                return promise.then(function () {
                  if(value !== undefined) {
                    return table.insert({ 
                      id: rowid, 
                      value: value
                    })
                  }
                })
              },
              Promise.resolve()
            )
          })
      }
    },
    retrieve: function (database, entityData, predicate) {
      var tableName = safeName(entityData.name, options.name)
      var table = database(tableName)
      return selectPredicate(table, entityData, predicate).select(tableName + '.*') //, true).select()
    },
    delete: function (database, entityData, predicate) {
      var table = database(safeName(entityData.name, options.name))
      return deletePredicate(table, entityData, predicate).delete()
    },
    clear: function (database, entityData) {
      return database(safeName(entityData.name, options.name)).delete()
    }
  }
}
