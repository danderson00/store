const expressions = require('@x/store.expressions')
const { unique, flatten } = require('@x/store/utilities/objects')
const { safeName } = require('./utilities')

module.exports = function (entity, database, options) {
  options = options || {}

  const indexes = unique(flatten(entity.indexes || []))
  const tableName = safeName(entity.name, options.name)

  if(options.sqlOnly) {
    const extractSql = builder => builder.toSQL().map(x => x.sql).join('\n')

    return [
      extractSql(createTable()),
      ...indexes.map(index => extractSql(createIndexTable(index)))
    ].join('\n')
  } else {
    return createTableIfNotExists()
      .then(createIndexTables)
  }

  function createTableIfNotExists() {
    return database.schema.hasTable(tableName)
      .then(exists => exists || createTable())
  }

  function createTable() {
    return database.schema.createTable(tableName, function (table) {
      if(entity.autoIncrement || !entity.keyPath) {
        table.increments('id')
      } else {
        table.string('id').primary()
      }
      table.text('content')
    })
  }

  function createIndexTables() {
    return indexes.reduce(function (promise, index) {
      return promise
        .then(() => createIndexTableIfNotExists(index))
    }, Promise.resolve())
  }

  function createIndexTableIfNotExists(index) {
    const indexTableName = tableName + '_' + safeName(index)

    return database.schema.hasTable(indexTableName)
      .then(exists => exists ||
        createIndexTable(index).then(() => insertIndexData(index))
      )
  }

  function createIndexTable(index) {
    const indexTableName = tableName + '_' + safeName(index)

    return database.schema.createTable(indexTableName, function (table) {
      if(entity.autoIncrement || !entity.keyPath) {
        table.integer('id').unsigned()
      } else {
        table.string('id')
      }

      // digitalocean managed mysql requires primary keys
      table.increments('index_id')
      table.string('value').nullable()
      table.index(['id', 'value'])
      table.foreign('id').references(`${tableName}.id`).onDelete('CASCADE')
    })
  }

  function insertIndexData(index) {
    return page(0, 20)

    function page(start, count) {
      return insertIndexRows(index, start, count)
        .then(function(result) {
          if(result !== false) {
            return page(start + count, count)
          }
        })
    }
  }

  function insertIndexRows(index, start, count) {
    const indexTableName = tableName + '_' + safeName(index)

    return database.table(tableName).offset(start).limit(count).select()
      .then(function(rows) {
        if(rows.length === 0) {
          return false
        }

        return Promise.all(rows.map(function (row) {
          const indexValue = expressions.evaluateKeyPath(index, JSON.parse(row.content))
          const values = (indexValue && indexValue.constructor === Array) ? indexValue : [indexValue]

          return database.table(indexTableName).insert(values.map(function (value) {
            return {
              id: row.id,
              value: value === undefined ? null : value
            }
          }))
        }))
      })
  }
}
