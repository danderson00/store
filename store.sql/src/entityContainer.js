var queriesModule = require('./queries')
var keyPath = require('@x/store.expressions/src/keyPath')
var api = require('@x/store.expressions/src/api')

module.exports = function (entityData, database, options) {
  var queries = queriesModule(options)
  return {
    store: function (entities) {
      return database.transaction(function (transaction) {
        if (entities.constructor === Array) {
          return entities.reduce(
            function (promise, entity) {
              return promise.then(function () {
                return storeEntity(entity)
              })
            },
            Promise.resolve()
          ).then(function () {
            return entities
          })
        }
        return storeEntity(entities)

        function storeEntity(entity) {
          return queries.store(transaction, entityData, entity)
            .then(function (rowid) {
              return queries.insertIndexes(transaction, entityData, entity, rowid)
                .then(function () {
                  if (entityData.keyPath && entityData.autoIncrement) {
                    keyPath.set(entityData.keyPath, entity, rowid)
                  }
                  return entity
                })
            })
        }
      })
    },
    retrieve: function (predicates) {
      if (predicates && predicates.constructor === Function) {
        predicates = api(predicates)
      }

      return queries.retrieve(database, entityData, predicates).then(function (rows) {
        // sqlite3 returns weird pseudo Arrays where array.constructor !== Array
        // calling Array.from works around this
        return Array.from(rows).map(function (row) {
          var result = JSON.parse(row.content)
          if (entityData.keyPath && entityData.autoIncrement)
            keyPath.set(entityData.keyPath, result, row.id)
          return result
        })
      })
    },
    delete: function (predicates) {
      if (predicates && predicates.constructor === Function) {
        predicates = api(predicates)
      }
      return queries.delete(database, entityData, predicates)
    },
    clear: function () {
      return queries.clear(database, entityData)
    }
  }
}
