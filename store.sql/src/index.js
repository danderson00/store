const initialise = require('./initialise')
const entityContainer = require('./entityContainer')
const knex = require('knex')

module.exports = function (options) {
  options = options || {}

  // suppress log spam by default, but we still need to log errors directly
  // as knex can explicitly exit the process if a driver module is missing
  // we need to correctly hook up external loggers here
  const log = {
    warn() { },
    error(message) {
      console.error(message)
    },
    deprecate() { },
    debug() { },
    ...options.log
  }

  const database = knex({ log: log , ...options })

  return {
    name: options.name,
    open: entities => (
      Promise.all(entities.map(entity => (
        initialise(entity, database, options)
      )))
    ),
    entityContainer: entity => (
      entityContainer(entity, database, options)
    ),
    initialisationSql: entities => ( 
      entities.map(entity =>
        initialise(entity, database, { ...options, sqlOnly: true })
      ).join('\n')
    ),
    close: () => database.destroy()
  }
}
