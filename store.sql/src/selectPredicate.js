var safeName = require('./utilities').safeName

//var predicate = {
//    p: 'path',      // path:        'path.to.property'
//    o: '=',         // operator:    '=', '!=', '>', '>=', '<', '<=', 'in'
//    v: 'value'      // value:       123, 'string', [1, 2, 3]
//}

module.exports = function (options) {
  return function (table, entityData, predicates, order) {
    if (predicates) {
      if (predicates.constructor === Array) {
        predicates.forEach(indexJoin)
        predicates.forEach(where)
      } else {
        indexJoin(predicates)
        where(predicates)
      }

      if(order) {
        orderBy()
      }
    }

    return table

    function orderBy() {
      if(predicates.constructor === Array) {
        predicates.forEach(function (predicate) {
          table.orderBy(columnName(predicate))
        })
      } else {
        table.orderBy(columnName(predicates))
      }
    }

    // support for undefined index values can be implemented in sql using
    // a special query that detects if the index value is not present (not just set to null)
    function indexJoin(predicate) {
      if (predicate.p !== entityData.keyPath) {
        var tableKey = safeName(entityData.name, options.name) + '.id'
        var indexKey = indexTableName(predicate) + '.id'

        table.innerJoin(indexTableName(predicate), tableKey, indexKey)
        table.orderBy(indexTableName(predicate) + '.value')
      }
    }

    function where(predicate) {
      table.where(columnName(predicate), sqlOperator(predicate), predicate.v === undefined ? null : predicate.v)// || null)
    }

    // duplicated in delete
    function sqlOperator(predicate) {
      var operator = predicate.o || '='
      if (operator === '=' && (predicate.v === undefined || predicate.v === null))
        return 'IS'
      if (operator === '=' && predicate.v && predicate.v.constructor === Array)
        return 'IN'
      return operator.toUpperCase()
    }

    function indexTableName(predicate) {
      return safeName(entityData.name + '_' + predicate.p, options.name)
    }

    function columnName(predicate) {
      return predicate.p === entityData.keyPath
        ? safeName(entityData.name, options.name) + '.id'
        : indexTableName(predicate) + '.value'
    }
  }
}
